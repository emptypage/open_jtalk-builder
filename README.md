# Open JTalk Builder on Docker

- [Project Home Page (Bitbucket)](https://bitbucket.org/emptypage/open_jtalk-builder)
- [emptypage/open_jtalk-builder (Docker Hub)](https://hub.docker.com/r/emptypage/open_jtalk-builder)

## What is this?

[Open JTalk](http://open-jtalk.sourceforge.net) is a Japanese TTS
(text-to-speech) system developed by HTS Working Group in NITECH (Nagoya
Institute of Technology Department of Computer Science).  It uses
[HTS Engine](http://hts-engine.sourceforge.net) (HMM-Based Speech Synthesis
Engine) library, which is also developed by them.

This Docker image builds the binary of the program and provides a handy
ready-to-use archive of the project.


## Usage

The image contains the tar archive of the built binaries in installable
directory layout as `open_jtalk-builder.tar.gz`.  You can see it by running the
image with arguments like:

```
% docker run --rm emptypage/open_jtalk-builder tar tf open_jtalk-builder.tar.gz
usr/
usr/local/
usr/local/lib/
usr/local/lib/libHTSEngine.a
usr/local/lib/open_jtalk/
...
```

When you run the image without any argument, it simply lists the file(s) in
`/root` directory.

```
% docker run --rm emptypage/open_jtalk-builder
total XXXXX
-rw-r--r-- 1 root root 71753866 Aug 28 07:27 open_jtalk-builder.tar.gz
```

You can get the archive with mounting your host directory (or another
container) as a volume like:

```
% mkdir tmp
% docker run --rm -v $PWD/tmp:/root/tmp emptypage/open_jtalk-builder cp open_jtalk-builder.tar.gz tmp
% ls tmp
open_jtalk-builder.tar.gz
```

In addition to that usage, the image is installed the binaries in its system,
so that you can use the `open_jtalk` (and `hts_engine`) command through the
image.

```
% echo こんにちは > tmp/a.txt
% docker run --rm -v $PWD/tmp:/root/tmp emptypage/open_jtalk-builder open_jtalk \
    -x /usr/local/lib/open_jtalk/dic \
    -m /usr/local/lib/open_jtalk/voice/mei/mei_normal.htsvoice \
    -ow tmp/a.wav tmp/a.txt
% ls tmp
a.txt				a.wav
```


## Files in "download" directory

These files are pre-downloaded souce code archives of the related projects
(Open JTalk and HTS engine API).  These projects look being hardly updated
now (2020), and they don't publish their souce code repository of the products
at present. The official download links for the products on the SourceForge.net
are slow, unstable for automation and inconvenient for most devlopers.  As I
checked that they are allowing redistribution of their products, I decided to
include them into the repository here as they are.

These files are downloaded from:

- `https://downloads.sourceforge.net/hts-engine/hts_engine_API-1.10.tar.gz`
- `https://downloads.sourceforge.net/open-jtalk/open_jtalk-1.11.tar.gz`
- `https://downloads.sourceforge.net/open-jtalk/open_jtalk_dic_utf_8-1.11.tar.gz`
- `https://downloads.sourceforge.net/open-jtalk/hts_voice_nitech_jp_atr503_m001-1.05.tar.gz`
- `https://downloads.sourceforge.net/mmdagent/MMDAgent_Example-1.8.zip`


The SHA-1 checksums of these files are:

```
76c1f8febff52f0ac4dabe9fe43490f98719804b  MMDAgent_Example-1.8.zip
2fdae559068f62117b8af1ba77be82535d9353a4  hts_voice_cmu_us_arctic_slt-1.06.tar.gz
4298eaef57f86b7c502488aad2f95963da75f061  hts_voice_nitech_jp_atr503_m001-1.05.tar.gz
de8f290664e3bfd7a071a0f6db9e949c4eedb306  open_jtalk-1.11.tar.gz
fea98651878ba5dcce90f2ba24b07e4732f199a8  open_jtalk_dic_utf_8-1.11.tar.gz
```

## Links

- [Open JTalk](http://open-jtalk.sourceforge.net)
- [HTS Engine](http://hts-engine.sourceforge.net)
- [MMDAgent](https://ja.osdn.net/projects/sfnet_mmdagent/)
