FROM ubuntu:22.04

ARG APT_INSTALL_PACKAGES="build-essential unzip"

RUN set -x && \
    apt-get -y update && \
    apt-get -y install $APT_INSTALL_PACKAGES && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /root
ARG HTS_ENGINE_API=hts_engine_API-1.10
ADD download/$HTS_ENGINE_API.tar.gz ./
RUN set -x && \
    cd $HTS_ENGINE_API && \
    ./configure --prefix=$HOME/usr/local && \
    make && \
    make install && \
    cd .. && \
    cp -r usr / && \
    rm -r $HTS_ENGINE_API*

WORKDIR /root
ARG OPEN_JTALK=open_jtalk-1.11
ADD download/$OPEN_JTALK.tar.gz ./
RUN set -x && \
    cd $OPEN_JTALK && \
    ./configure --prefix=$HOME/usr/local && \
    make && \
    make install && \
    cd .. && \
    rm -r $OPEN_JTALK*

WORKDIR /root
ARG OPEN_JTALK_DIC=open_jtalk_dic_utf_8-1.11
ADD download/$OPEN_JTALK_DIC.tar.gz ./
RUN set -x && \
    mkdir -p $HOME/usr/local/lib/open_jtalk/dic && \
    cp $OPEN_JTALK_DIC/* $HOME/usr/local/lib/open_jtalk/dic && \
    rm -r $OPEN_JTALK_DIC*

WORKDIR /root
ARG HTS_VOICE_NITECH=hts_voice_nitech_jp_atr503_m001-1.05
ADD download/$HTS_VOICE_NITECH.tar.gz ./
RUN set -x && \
    mkdir -p $HOME/usr/local/lib/open_jtalk/voice/nitech && \
    cp $HTS_VOICE_NITECH/* $HOME/usr/local/lib/open_jtalk/voice/nitech && \
    rm -r $HTS_VOICE_NITECH*

WORKDIR /root
ARG MMDAGENT_EXAMPLE=MMDAgent_Example-1.8
ADD download/$MMDAGENT_EXAMPLE.zip ./
RUN set -x && \
    unzip $MMDAGENT_EXAMPLE.zip && \
    mkdir -p $HOME/usr/local/lib/open_jtalk/voice/mei && \
    cp $MMDAGENT_EXAMPLE/Voice/mei/* $HOME/usr/local/lib/open_jtalk/voice/mei && \
    mkdir -p $HOME/usr/local/lib/open_jtalk/voice/slt && \
    cp $MMDAGENT_EXAMPLE/Voice/slt/* $HOME/usr/local/lib/open_jtalk/voice/slt && \
    mkdir -p $HOME/usr/local/lib/open_jtalk/voice/takumi && \
    cp $MMDAGENT_EXAMPLE/Voice/takumi/* $HOME/usr/local/lib/open_jtalk/voice/takumi && \
    rm -r $MMDAGENT_EXAMPLE $MMDAGENT_EXAMPLE.zip

WORKDIR /root
RUN set -x && \
    tar zcvf $HOME/open_jtalk-builder.tar.gz usr && \
    cp -r usr / && \
    rm -r usr

CMD ls -lF
