.PHONY: build buildx

TAG=22.04-1.11

build:
	docker build \
	  -t emptypage/open_jtalk-builder:${TAG} \
	  .

buildx:
	docker buildx build --platform linux/amd64,linux/arm64 \
	  -t emptypage/open_jtalk-builder:${TAG} \
	  -t emptypage/open_jtalk-builder:latest \
	  --push .
